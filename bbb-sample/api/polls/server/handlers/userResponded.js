import { check } from 'meteor/check';
import Polls from '/imports/api/polls';
import Users from '/imports/api/users';
import Logger from '/imports/startup/server/logger';
import { extractCredentials } from '/imports/api/common/server/helpers';

export default function userResponded({ body }) {
  const { pollId, userId, answerId } = body;
  check(pollId, String);
  check(userId, String);
  check(answerId, Number);

  const selector = {
    id: pollId,
  };
  const { meetingId, requesterUserId } = extractCredentials(this.userId);
  let current = Polls.findOne(selector)
  let userAnswer = current.answers.filter(item=>item.id==answerId)
  let user = Users.findOne({userId})
  const modifier = {
    $pull: {
      users: userId,
    },
    $push: {
      responses: { userId, answerId ,grade:current.correctAnswer==userAnswer[0].key?current.grade:0,userName:user.name},
    },
  };

  const cb = (err) => {
    if (err) {
      return Logger.error(`Updating Poll responses: ${err}`);
    }

    return Logger.info(`Updating Poll response (userId: ${userId},`
      + `response: ${answerId}, pollId: ${pollId})`);
  };

  return Polls.update(selector, modifier, cb);
}
