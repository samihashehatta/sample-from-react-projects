import Polls from '/imports/api/polls';
import Logger from '/imports/startup/server/logger';

export default function addPollQuestion(meetingId, requesterId, pollId,others) {
  
  const selector = {
    meetingId,
    requester: requesterId,
    id:pollId,
  };
  const modifier = Object.assign(
    { meetingId },
    { requester: requesterId },
    {question:true},
    {questionString:others.question},
    {correctAnswer:others.correctAnswer},
    {grade:others.grade},
    {usersToQuiz:others.users},
    {pollId},
   { id:pollId},

  );

  const cb = (err, numChanged) => {
    if (err != null) {
      return Logger.error(`Adding Poll to collection: ${pollId}`);
    }

    const { insertedId } = numChanged;
    if (insertedId) {
      return Logger.info(`Added Poll id=${pollId}`);
    }

    return Logger.info(`Upserted Poll id=${pollId}`);
  };

  return Polls.upsert(selector, modifier, cb);
}
