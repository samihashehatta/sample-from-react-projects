import Users from '/imports/api/users';
import Polls from '/imports/api/polls';
import Logger from '/imports/startup/server/logger';
import flat from 'flat';
import { check } from 'meteor/check';

export default function addPoll(meetingId, requesterId, poll) {
  check(requesterId, String);
  check(meetingId, String);
  check(poll, {
    id: String,
    answers: [
      {
        id: Number,
        key: String,
      },
    ],
  });
  const userSelector = {
    meetingId,
    userId: { $ne: requesterId },
    clientType: { $ne: 'dial-in-user' },
  };

  const userIds = Users.find(userSelector, { fields: { userId: 1 } })
    .fetch()
    .map(user => user.userId);

  const selector = {
    meetingId,
    requester: requesterId,
    id: poll.id,
  };

  const modifier = Object.assign(
    { meetingId },
    { requester: requesterId },
    { users: userIds },
    {question:false},
    {active:true},
    flat(poll, { safe: true }),
  );

  const cb = (err, numChanged) => {
    if (err != null) {
      return Logger.error(`Adding Poll to collection: ${poll.id}`);
    }

    const { insertedId } = numChanged;
    if (insertedId) {
      return Logger.info(`Added Poll id=${poll.id}`);
    }

    return Logger.info(`Upserted Poll id=${poll.id}`);
  };
  const quId = poll.id.split('/',2).join('/')
  let question = Polls.findOne({question:true,id:quId})
  modifier.questionString = question.questionString
  modifier.correctAnswer = question.correctAnswer
  modifier.grade = question.grade
  modifier.usersToQuiz = question.usersToQuiz

  return Polls.upsert(selector, modifier, cb);
}
