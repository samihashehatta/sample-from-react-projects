import Polls from '/imports/api/polls';
import Logger from '/imports/startup/server/logger';

export default function clearPolls(meetingId) {
  if (meetingId) {
    return Polls.update({ meetingId },{$set:{active:false}}, () => {
      Logger.info(`Cleared Polls (${meetingId})`);
    });
  }

  return Polls.update({},{$set:{active:false}}, () => {
    Logger.info('Cleared Polls (all)');
  });
}
