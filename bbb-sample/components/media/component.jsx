import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import WebcamDraggable from './webcam-draggable-overlay/component';
import Tabs from '../tabs/component';

import { styles } from './styles';

const propTypes = {
  children: PropTypes.element.isRequired,
  usersVideo: PropTypes.arrayOf(Array),
  singleWebcam: PropTypes.bool.isRequired,
  hideOverlay: PropTypes.bool,
  swapLayout: PropTypes.bool,
  disableVideo: PropTypes.bool,
  userWasInWebcam: PropTypes.bool,
  audioModalIsOpen: PropTypes.bool,
  joinVideo: PropTypes.func,
  webcamPlacement: PropTypes.string,
};

const defaultProps = {
  usersVideo: [],
  hideOverlay: true,
  swapLayout: false,
  disableVideo: false,
  userWasInWebcam: false,
  audioModalIsOpen: false,
  joinVideo: null,
  webcamPlacement: 'top',
};


export default class Media extends Component {
  constructor(props) {
    super(props);
    this.refContainer = React.createRef();
    this.renderCameras =this.renderCameras.bind(this)
  }

  componentWillUpdate() {
    window.dispatchEvent(new Event('resize'));
  }

  componentDidUpdate(prevProps) {
    const {
      userWasInWebcam,
      audioModalIsOpen,
      joinVideo,
    } = this.props;

    const {
      audioModalIsOpen: oldAudioModalIsOpen,
    } = prevProps;

    if ((!audioModalIsOpen && oldAudioModalIsOpen) && userWasInWebcam) {
      Session.set('userWasInWebcam', false);
      joinVideo();
    }
  }
  
  componentWillReceiveProps(nextProps){
    const {pollInitiated , isScreensharing ,extrenalVid ,special} = nextProps
    if( isScreensharing || extrenalVid ) {
      Session.set('cameraLayout',false)
      Session.set('cameraMood','default')
    }
  }
  renderCameras(){
  const { 
    swapLayout,
    singleWebcam,
    hideOverlay,
    disableVideo,
    audioModalIsOpen,
    usersVideo,
    userUno,
    presenter,
    cameraLayout,
    cameraMood
  } = this.props;
  const usersWithOutPresenter = usersVideo.filter(user=>user.userId != presenter.userId)
  const presenterArray = usersVideo.filter(user=>user.userId == presenter.userId)
    return(
      <>
      {usersVideo.length>0?
        <WebcamDraggable
          userUno={true}
          refMediaContainer={this.refContainer}
          cameraMood={cameraMood}
          swapLayout={swapLayout}
          singleWebcam={singleWebcam}
          usersVideoLenght={usersVideo.length}
          hideOverlay={hideOverlay}
          disableVideo={disableVideo}
          audioModalIsOpen={audioModalIsOpen}
          usersVideo={usersVideo}
          presenter={presenter}
        />
        : <div></div>
      }
    </>
    )
  }
  render() {
    const {
      hideOverlay,
      webcamPlacement,
      userUno,
      presenter,
      isScreensharing,
      intl,
      currentPresentation,
      cameraMood,
      cameraLayout,
    } = this.props;
 
    let openTab=Session.get('openTab')
    let screenshare=Session.get('screenshare')
    const contentClassName = cx({
      [styles.content]: true,
    });

    const overlayClassName = cx({
      [styles.overlay]: true,
      [styles.hideOverlay]: hideOverlay,
      [styles.floatingOverlay]: (webcamPlacement === 'floating'),
    });
 

        return (
          <div
            id="container"
            className={cx(styles.container)}
            ref={this.refContainer}
         
          >

          
              
              {userUno?
              this.renderCameras():
              <Tabs
              style={{width:'100%',height:'100%'}}
              openTab={openTab}
              screenshare={screenshare}
              refCont={this.refContainer}
              presenter={presenter}
              intl={intl}
              amIPresenter
              amIModerator
              isPollingEnabled
              allowExternalVideo
              handleTakePresenter
              isSharingVideo
              stopExternalVideoShare
              isMeteorConnected
              cameraLayout
              currentPresentationName={currentPresentation?currentPresentation.name:'default.pdf'}
              {...this.props}
              />

              }
            </div>
       
        );
      
   
  }
}

Media.propTypes = propTypes;
Media.defaultProps = defaultProps;
