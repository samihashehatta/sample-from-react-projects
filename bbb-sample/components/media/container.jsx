import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import Settings from '/imports/ui/services/settings';
import { defineMessages, injectIntl, intlShape } from 'react-intl';
import PropTypes from 'prop-types';
import { Session } from 'meteor/session';
import { notify } from '/imports/ui/services/notification';
import VideoService from '/imports/ui/components/video-provider/service';
import getFromUserSettings from '/imports/ui/services/users-settings';
import { withModalMounter } from '/imports/ui/components/modal/service';
import Media from './component';
import MediaService, { getSwapLayout, shouldEnableSwapLayout } from './service';
import PresentationPodsContainer from '../presentation-pod/container';
import ScreenshareContainer from '../screenshare/container';
import DefaultContent from '../presentation/default-content/component';
import ExternalVideoContainer from '../external-video-player/container';
import Storage from '../../services/storage/session';
import Users from '/imports/api/users';
import ActionService from '../actions-bar/service'
import { makeCall } from '/imports/ui/services/api';

const LAYOUT_CONFIG = Meteor.settings.public.layout;
const KURENTO_CONFIG = Meteor.settings.public.kurento;
import ExternalVideoService from '/imports/ui/components/external-video-player/service';

const propTypes = {
  isScreensharing: PropTypes.bool.isRequired,
  userUno: PropTypes.bool.isRequired,
  tabs: PropTypes.string.isRequired,
  intl: intlShape.isRequired,
};

const intlMessages = defineMessages({
  screenshareStarted: {
    id: 'app.media.screenshare.start',
    description: 'toast to show when a screenshare has started',
  },
  screenshareEnded: {
    id: 'app.media.screenshare.end',
    description: 'toast to show when a screenshare has ended',
  },
  screenshareNotSupported: {
    id: 'app.media.screenshare.notSupported',
    description: 'Error message for screenshare not supported',
  },
  chromeExtensionError: {
    id: 'app.video.chromeExtensionError',
    description: 'Error message for Chrome Extension not installed',
  },
  chromeExtensionErrorLink: {
    id: 'app.video.chromeExtensionErrorLink',
    description: 'Error message for Chrome Extension not installed',
  },
});

class MediaContainer extends Component {
  componentWillMount() {
    document.addEventListener('installChromeExtension', this.installChromeExtension.bind(this));
    document.addEventListener('screenshareNotSupported', this.screenshareNotSupported.bind(this));
  }
  componentWillReceiveProps(nextProps) {
    const {
      isScreensharing,
      intl,
      userUno
    } = this.props;

    if (isScreensharing !== nextProps.isScreensharing && !userUno) {
      if (nextProps.isScreensharing ) {
        notify(intl.formatMessage(intlMessages.screenshareStarted), 'info', 'desktop');
      } else {
        notify(intl.formatMessage(intlMessages.screenshareEnded), 'info', 'desktop');
      }
    }
  }

  componentWillUnmount() {
    document.removeEventListener('installChromeExtension', this.installChromeExtension.bind(this));
    document.removeEventListener('screenshareNotSupported', this.screenshareNotSupported.bind(this));
  }

  installChromeExtension() {
    const { intl } = this.props;

    const CHROME_DEFAULT_EXTENSION_LINK = KURENTO_CONFIG.chromeDefaultExtensionLink;
    const CHROME_CUSTOM_EXTENSION_LINK = KURENTO_CONFIG.chromeExtensionLink;
    const CHROME_EXTENSION_LINK = CHROME_CUSTOM_EXTENSION_LINK === 'LINK' ? CHROME_DEFAULT_EXTENSION_LINK : CHROME_CUSTOM_EXTENSION_LINK;

    const chromeErrorElement = (
      <div>
        {intl.formatMessage(intlMessages.chromeExtensionError)}
        {' '}
        <a href={CHROME_EXTENSION_LINK} target="_blank" rel="noopener noreferrer">
          {intl.formatMessage(intlMessages.chromeExtensionErrorLink)}
        </a>
      </div>
    );
    notify(chromeErrorElement, 'error', 'desktop');
  }

  screenshareNotSupported() {
    const { intl } = this.props;
    notify(intl.formatMessage(intlMessages.screenshareNotSupported), 'error', 'desktop');
  } 

  render() {
  
    return <Media {...this.props} />;
  }
}

export default withModalMounter(withTracker(() => {
  const { dataSaving } = Settings;
  const { viewParticipantsWebcams, viewScreenshare } = dataSaving;
  const hidePresentation = getFromUserSettings('bbb_hide_presentation', LAYOUT_CONFIG.hidePresentation);
  const { current_presentation: hasPresentation } = MediaService.getPresentationInfo();
  const data = {
    children: <DefaultContent />,
    audioModalIsOpen: Session.get('audioModalIsOpen'),
    userWasInWebcam: Session.get('userWasInWebcam'),
    joinVideo: VideoService.joinVideo,
  };
  data.allPresent = MediaService.getAllPresentations()
  data.whiteBoards = MediaService.getAllWhiteboard()
  console.log('alllll',data.whiteBoards)
  if (MediaService.shouldShowWhiteboard() && !hidePresentation) {
    data.currentPresentation = MediaService.getPresentationInfo();
   data.children = <PresentationPodsContainer />;
}
  data.currentPresentation = MediaService.getPresentationAllInfo();
  data.shouldShowWhiteboard =MediaService.shouldShowWhiteboard()
  if (MediaService.shouldShowScreenshare() && (viewScreenshare || MediaService.isUserPresenter())) {
    data.children = <ScreenshareContainer />;
  }

  const usersVideo = VideoService.getAllWebcamUsers();
  data.usersVideo = usersVideo;

  if (MediaService.shouldShowOverlay() && usersVideo.length && viewParticipantsWebcams) {
    data.floatingOverlay = usersVideo.length < 2;
    data.hideOverlay = usersVideo.length === 0;
  }
  const presenter =  Users.findOne({ presenter: true }, {
    fields: {
      name: 1,
      userId: 1,
      role: 1,
      emoji: 1,
      clientType: 1,
      presenter:1,
    },
  })
  let newUsers = usersVideo.filter((user)=>user.userId!=presenter.userId)
  data.singleWebcam = (newUsers.length < 2);
  data.presenter=presenter
  data.isScreensharing = MediaService.isVideoBroadcasting();
  data.swapLayout = (getSwapLayout() || !hasPresentation) && shouldEnableSwapLayout();
  data.disableVideo = !viewParticipantsWebcams;

  if (data.swapLayout) {
    data.floatingOverlay = true;
    data.hideOverlay = true;
  }
  data.extrenalVid=MediaService.shouldShowExternalVideo()
  data.isUserPresenter=MediaService.isUserPresenter()
  if (MediaService.shouldShowExternalVideo()) {
    data.children = (
      <ExternalVideoContainer
        isPresenter={MediaService.isUserPresenter()}
      />
    );
  }
  const POLLING_ENABLED = Meteor.settings.public.poll.enabled;

  data.special = Session.get('special')
  data.pollInitiated=Session.get('pollInitiated')
  data.isPollOpen = Session.get('isPollOpen')
  data.cameraLayout = Session.get('cameraLayout')
  data.cameraMood = Session.get('cameraMood')
  data.quraan = Session.equals('openTab','quranVerse')
  data.openPdf = Session.get('openPdf')
  data.openCodeTab = Session.get('openCodeTab')
  data.pubChat = Session.get('pubChat')
  data.openCameras=Session.get('openCameras')
  data.amIPresenter= ActionService.amIPresenter(),
  data.amIModerator= ActionService.amIModerator(),
  data.openNewWhite=Session.get('openNewWhite')
  data.openPoll=Session.get('openPoll')
  data.removeFrom=Session.get('removeFrom')
  data.notes = !Session.get('cameraLayout')
  data.isPollingEnabled= POLLING_ENABLED,
  data.allowExternalVideo= Meteor.settings.public.externalVideoPlayer.enabled,
  data.handleTakePresenter= ActionService.takePresenterRole,
  data.isSharingVideo= ActionService.isSharingVideo(),
  data.stopExternalVideoShare= ExternalVideoService.stopWatching,
  data.isMeteorConnected= Meteor.status().connected,
  data.forcePollOpen=Session.get('forcePollOpen')
  data.webcamPlacement = Storage.getItem('webcamPlacement');
  MediaContainer.propTypes = propTypes;
  
  return data;
})(injectIntl(MediaContainer)));
