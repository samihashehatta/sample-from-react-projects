import React from 'react';
import { makeCall } from '/imports/ui/services/api';
import { withTracker } from 'meteor/react-meteor-data';
import Auth from '/imports/ui/services/auth';
import Presentations from '/imports/api/presentations';
import PresentationAreaService from '/imports/ui/components/presentation/service';
import Poll from '/imports/ui/components/poll/component';
import Service from './service';
import { withModalMounter } from '/imports/ui/components/modal/service';
import service from './live-result/service';
const PollContainer = ({ ...props }) => <Poll {...props} />;

// export default withTracker(() => {
//   Meteor.subscribe('current-poll');

//   const currentPresentation = Presentations.findOne({
//     current: true,
//   }, { fields: { podId: 1 } }) || {};

//   const currentSlide = PresentationAreaService.getCurrentSlide(currentPresentation.podId);

//   const startPoll = type => makeCall('startPoll', type, currentSlide.id);

//   const startCustomPoll = (type, answers) => makeCall('startPoll', type, currentSlide.id, answers);

//   return {
//     currentSlide,
//     amIPresenter: Service.amIPresenter(),
//     pollTypes: Service.pollTypes,
//     startPoll,
//     startCustomPoll,
//     stopPoll: Service.stopPoll,
//     publishPoll: Service.publishPoll,
//     currentPoll: Service.currentPoll(),
//     resetPollPanel: Session.get('resetPollPanel') || false,
//     pollAnswerIds: Service.pollAnswerIds,
//     isMeteorConnected: Meteor.status().connected,
//   };
// })(PollContainer);

export default withTracker(({ mountModal }) =>{ 
    Meteor.subscribe('current-poll');

  const currentPresentation = Presentations.findOne({
    current: true,
  }, { fields: { podId: 1 } }) || {};

  const currentSlide = PresentationAreaService.getCurrentSlide(currentPresentation.podId);

  const startPoll = (type,q) =>  makeCall('startPoll', type, currentSlide.id,[],q);

  const startCustomPoll = (type, answers,q) => makeCall('startPoll', type, currentSlide.id, answers,q);
  return({
  closeModal: () => {
    mountModal(null);
  },
  mountModal,
  currentSlide,
  amIPresenter: Service.amIPresenter(),
  pollTypes: Service.pollTypes,
  pollTypesWithIds: Service.pollTypesWithIds,
  users: Service.users(),
  startPoll,
  startCustomPoll,
  stopPoll: Service.stopPoll,
  publishPoll: Service.publishPoll,
  currentPoll: Service.currentPoll(),
  resetPollPanel: Session.get('resetPollPanel') || false,
  pollAnswerIds: Service.pollAnswerIds,
  isMeteorConnected: Meteor.status().connected,
  })
})(PollContainer);
