import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { defineMessages, injectIntl } from 'react-intl';
import PresentationUploaderContainer from '/imports/ui/components/presentation/presentation-uploader/container';
import { withModalMounter } from '/imports/ui/components/modal/service';
import Modal from '/imports/ui/components/modal/simple/component';
import _ from 'lodash';
import { Session } from 'meteor/session';
import Button from '/imports/ui/components/button/component';
import LiveResult from './live-result/component';
import { styles } from './styles.scss';
import { pollAnswerIds } from './service';

const intlMessages = defineMessages({
  pollPaneTitle: {
    id: 'app.poll.pollPaneTitle',
    description: 'heading label for the poll menu',
  },
  closeLabel: {
    id: 'app.poll.closeLabel',
    description: 'label for poll pane close button',
  },
  hidePollDesc: {
    id: 'app.poll.hidePollDesc',
    description: 'aria label description for hide poll button',
  },
  customPollLabel: {
    id: 'app.poll.customPollLabel',
    description: 'label for custom poll button',
  },
  startCustomLabel: {
    id: 'app.poll.startCustomLabel',
    description: 'label for button to start custom poll',
  },
  customPollInstruction: {
    id: 'app.poll.customPollInstruction',
    description: 'instructions for using custom poll',
  },
  quickPollInstruction: {
    id: 'app.poll.quickPollInstruction',
    description: 'instructions for using pre configured polls',
  },
  activePollInstruction: {
    id: 'app.poll.activePollInstruction',
    description: 'instructions displayed when a poll is active',
  },
  ariaInputCount: {
    id: 'app.poll.ariaInputCount',
    description: 'aria label for custom poll input field',
  },
  customPlaceholder: {
    id: 'app.poll.customPlaceholder',
    description: 'custom poll input field placeholder text',
  },
  noPresentationSelected: {
    id: 'app.poll.noPresentationSelected',
    description: 'no presentation label',
  },
  clickHereToSelect: {
    id: 'app.poll.clickHereToSelect',
    description: 'open uploader modal button label',
  },
  tf: {
    id: 'app.poll.tf',
    description: 'label for true / false poll',
  },
  yn: {
    id: 'app.poll.yn',
    description: 'label for Yes / No poll',
  },
  a2: {
    id: 'app.poll.a2',
    description: 'label for A / B poll',
  },
  a3: {
    id: 'app.poll.a3',
    description: 'label for A / B / C poll',
  },
  a4: {
    id: 'app.poll.a4',
    description: 'label for A / B / C / D poll',
  },
  a5: {
    id: 'app.poll.a5',
    description: 'label for A / B / C / D / E poll',
  },
  true: {
    id: 'app.poll.answer.true',
    description: 'label for poll answer True',
  },
  false: {
    id: 'app.poll.answer.false',
    description: 'label for poll answer False',
  },
  yes: {
    id: 'app.poll.answer.yes',
    description: 'label for poll answer Yes',
  },
  no: {
    id: 'app.poll.answer.no',
    description: 'label for poll answer No',
  },
  a: {
    id: 'app.poll.answer.a',
    description: 'label for poll answer A',
  },
  b: {
    id: 'app.poll.answer.b',
    description: 'label for poll answer B',
  },
  c: {
    id: 'app.poll.answer.c',
    description: 'label for poll answer C',
  },
  d: {
    id: 'app.poll.answer.d',
    description: 'label for poll answer D',
  },
  e: {
    id: 'app.poll.answer.e',
    description: 'label for poll answer E',
  },
  e: {
    id: 'app.poll.answer.e',
    description: 'label for poll answer E',
  },
  question: {
    id: 'app.poll.question',
    description: 'label for poll answer E',
  },
  grade: {
    id: 'app.poll.grade',
    description: 'label for poll answer E',
  },
  correct: {
    id: 'app.poll.correct',
    description: 'label for poll answer E',
  },
  start: {
    id: 'app.poll.start',
    description: 'label for poll answer E',
  },
  users: {
    id: 'app.poll.users',
    description: 'label for poll answer E',
  },
  clear: {
    id: 'app.whiteboard.clear',
    description: 'label for poll answer E',
  },
  default: {
    id: 'app.poll.default',
    description: 'label for poll answer E',
  },
});


const MAX_CUSTOM_FIELDS = Meteor.settings.public.poll.max_custom;
const MAX_INPUT_CHARS = 45;

class Poll extends Component {
  constructor(props) {
    super(props);

    this.state = {
      customPollReq: false,
      isPolling: false,
      customPollValues: [],
      question:'',
      type:null,
      showCorrectAnswer:false,
      correctAnswer:'',
      grade:0,
      users:[]
    };

    this.inputEditor = [];

    this.toggleCustomFields = this.toggleCustomFields.bind(this);
    this.renderQuickPollBtns = this.renderQuickPollBtns.bind(this);
    this.renderCustomView = this.renderCustomView.bind(this);
    this.renderInputFields = this.renderInputFields.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleBackClick = this.handleBackClick.bind(this);
    this.renderCorrectAndGrade=this.renderCorrectAndGrade.bind(this)
    this.changeType=this.changeType.bind(this)
    this.handleStartPoll=this.handleStartPoll.bind(this)
    this.handleGrade=this.handleGrade.bind(this)
    this.handleCorrectAnswer=this.handleCorrectAnswer.bind(this)
    this.handleUsersToQuiz=this.handleUsersToQuiz.bind(this)
    
  }

  // componentDidMount() {
  //   // const { props } = this.hideBtn;
  //   // const { className } = props;

  //   // const hideBtn = document.getElementsByClassName(`${className}`);
  //   // if (hideBtn[0]) hideBtn[0].focus();
  // }

  componentDidUpdate() {
    const { amIPresenter } = this.props;

    if (Session.equals('resetPollPanel', true)) {
      this.handleBackClick();

    }

    if (!amIPresenter) {
      Session.set('openPanel', 'userlist');
      Session.set('forcePollOpen', false);
    }
  }

  handleInputChange(index, event) {
    // This regex will replace any instance of 2 or more consecutive white spaces
    // with a single white space character.
    const option = event.target.value.replace(/\s{2,}/g, ' ').trim();

    this.inputEditor[index] = option === '' ? '' : option;

    this.setState({ customPollValues: this.inputEditor });
  }
   handleQuChange( event) {
    // This regex will replace any instance of 2 or more consecutive white spaces
    // with a single white space character.
    this.setState({ question: event.target.value });
  }

  handleBackClick() {
    const { stopPoll } = this.props;
    Session.set('resetPollPanel', false);
    // Session.set('qu','')

    stopPoll();
    this.inputEditor = [];
    this.setState({
      isPolling: false,
      customPollValues: this.inputEditor,
      question:'',
      type:null,
      showCorrectAnswer:false,
      correctAnswer:'',
      grade:0,
      users:[]
      
    }, document.activeElement.blur());
  }
  changeType (type){
    this.setState({type,showCorrectAnswer:true})
  }
  handleCorrectAnswer (event){
    this.setState({correctAnswer:event.target.value})
  }
  handleUsersToQuiz (event){
    const {users} = this.state
    let val = event.target.value
    let filterd =[]
    if(users.includes(val) && users.length>1)  {
      filterd = users.filter(id=>id!=val)
      this.setState({users:filterd})
    }
    else {
      users.push(val)
      this.setState({users})
    }
    
   
  }
  handleGrade (event){
    let grade = event.target.value < 0? 0:event.target.value;
    this.setState({grade})
  }
  toggleCustomFields() {
    const { customPollReq } = this.state;
    return this.setState({ customPollReq: !customPollReq });
  }

  renderQuickPollBtns() {
    const {
      isMeteorConnected, pollTypes, startPoll, intl,
    } = this.props;

    const {question}=this.state

    const btns = pollTypes.map((type) => {
      if (type === 'custom') return false;

      const label = intl.formatMessage(
        // regex removes the - to match the message id
        intlMessages[type.replace(/-/g, '').toLowerCase()],
      );

      return (
        <Button
          disabled={!isMeteorConnected}
          label={label}
          color="default"
          className={styles.pollBtn}
          key={_.uniqueId('quick-poll-')}
          onClick={() => this.changeType(type)}
        />);
    });

    return btns;
  }

  renderCustomView() {
    const { intl, startCustomPoll } = this.props;
   // _.compact(this.inputEditor)
    const isDisabled = _.compact(this.inputEditor).length < 1;
    const {question} = this.state
    return (
      <div className={styles.customInputWrapper}>
        {this.renderInputFields()}

        <Button
          onClick={() => this.changeType('custom')}
          label={intl.formatMessage(intlMessages.correct)}
          color="primary"
          aria-disabled={isDisabled}
          disabled={isDisabled}
          className={styles.btn}
        />
      
      </div>
    );
  }

  renderInputFields() {
    const { intl } = this.props;
    const { customPollValues } = this.state;
    let items = [];

    items = _.range(1, MAX_CUSTOM_FIELDS + 1).map((ele, index) => {
      const id = index;
      return (
        <div key={`custom-poll-${id}`} className={styles.pollInput}>
          <input
            aria-label={intl.formatMessage(
              intlMessages.ariaInputCount, { 0: id + 1, 1: MAX_CUSTOM_FIELDS },
            )}
            placeholder={intl.formatMessage(intlMessages.customPlaceholder)}
            className={styles.input}
            onChange={event => this.handleInputChange(id, event)}
            defaultValue={customPollValues[id]}
            maxLength={MAX_INPUT_CHARS}
          />
        </div>
      );
    });

    return items;
  }

  renderActivePollOptions() {
    const {
      intl,
      isMeteorConnected,
      stopPoll,
      currentPoll,
      pollAnswerIds,
    } = this.props;
    return (
      <div>
        <div className={styles.instructions}>
          {intl.formatMessage(intlMessages.activePollInstruction)}
        </div>
        <LiveResult
          {...{
            isMeteorConnected,
            stopPoll,
            currentPoll,
            pollAnswerIds,
          }}
          handleBackClick={this.handleBackClick}
        />
      </div>
    );
  }
  renderCorrectAndGrade (){
    const {customPollValues,type} = this.state 
    const {intl,pollTypesWithIds} = this.props
    let toChooseFrom = []
    if (type=='custom') {
      toChooseFrom = customPollValues.map(item=>(
        <option key={item} value={item}>
                {item}
        </option>
      ))
    }
    else{ 
      toChooseFrom = pollTypesWithIds[type].map(item=>{
      return(
        <option key={item}>
                {intl.formatMessage(intlMessages[item])}
        </option>
      )
      })
    }
    return(
      <>
      
      <select className={styles.select}            
      onChange={event => this.handleCorrectAnswer(event)}
      >
         <option key={'test'} value={null}>
                {intl.formatMessage(intlMessages.default)}
        </option>
        {toChooseFrom}
      </select>
      <input
            placeholder={intl.formatMessage(intlMessages.grade)}
            className={styles.grade}
            type="number"
            min={0}
            onChange={event => this.handleGrade(event)}
            defaultValue={0}
            maxLength={MAX_INPUT_CHARS}
        />
      </>
    )

  }
  handleStartPoll (){
    const {startPoll,startCustomPoll} = this.props
    const {question,type,customPollValues,grade,correctAnswer,users} = this.state
    this.setState({isPolling:true})
    let otherData={
      question ,
      grade,
      correctAnswer,
      users
    }
    Session.set('pollInitiated',true)
    if(type=='custom') startCustomPoll('custom',customPollValues,otherData)
    else startPoll(type,otherData)
    
   }
   handleClearPoll (){
    Session.set('clearPoll',true)
   }
  renderPollOptions() {
    const { isMeteorConnected, intl ,isDisabled , users} = this.props;
    const { customPollReq , showCorrectAnswer } = this.state;

    let userOptions = users.length>0?users.map( user =>{
      return(
        
        <label htmlFor={`${user.userId}`} className={styles.freeJoinLabel} key={user.userId}>
        <input
          type="checkbox"
          value={user.userId}
          className={styles.freeJoinCheckbox}
          onChange={this.handleUsersToQuiz}
          id={user.userId}
        />
        <span aria-hidden>{user.name}</span>
      </label>
      )
    }):[]

    return (
      <div className={styles.pollC}>
        <div>
             <input
            placeholder={intl.formatMessage(intlMessages.question)}
            className={styles.inputHeader}
            onChange={event => this.handleQuChange(event)}
            defaultValue={''}
            maxLength={MAX_INPUT_CHARS}
            />
          
        </div>
        <div className={styles.instructions}>
          {intl.formatMessage(intlMessages.quickPollInstruction)}
        </div>
        <div className={styles.grid}>
          {this.renderQuickPollBtns()}
        </div>
        <div className={styles.instructions}>
          {intl.formatMessage(intlMessages.customPollInstruction)}
        </div>
       
        <Button
          disabled={!isMeteorConnected}
          className={styles.customBtn}
          color="default"
          onClick={this.toggleCustomFields}
          label={intl.formatMessage(intlMessages.customPollLabel)}
          aria-expanded={customPollReq}
        />
        {!customPollReq ? null : this.renderCustomView()}
        <div className={styles.instructions}>
          {showCorrectAnswer?intl.formatMessage(intlMessages.correct):null}
        </div>
        <div className={styles.grid2}>
        {showCorrectAnswer?this.renderCorrectAndGrade():null}
        </div>
       
          <div className={styles.instructions}>{users.length>1?intl.formatMessage(intlMessages.users):null}</div>
        
          <div className={styles.checkBoxesContainer}>
          {
            userOptions
          }
         </div>
         <Button
          onClick={this.handleStartPoll}
          label={intl.formatMessage(intlMessages.start)}
          color="primary"
          aria-disabled={isDisabled}
          disabled={isDisabled}
          className={styles.customBtn}
        />
        
 
      </div>
    );
  }

  renderNoSlidePanel() {
    const { mountModal, intl } = this.props;
    return (
      <div className={styles.noSlidePanelContainer}>
        <h4>{intl.formatMessage(intlMessages.noPresentationSelected)}</h4>
        <Button
          label={intl.formatMessage(intlMessages.clickHereToSelect)}
          color="primary"
          onClick={() => mountModal(<PresentationUploaderContainer />)}
          className={styles.pollBtn}
        />
      </div>
    );
  }

  renderPollPanel() {
    const { isPolling } = this.state;
    const {
      currentPoll,
      currentSlide,
    } = this.props;

    if (!currentSlide) return this.renderNoSlidePanel();

    if (isPolling || (!isPolling && currentPoll)) {
      return this.renderActivePollOptions();
    }

    return this.renderPollOptions();
  }

  render() {
    const {
      intl,
      stopPoll,
      currentPoll,
      amIPresenter,
      closeModal
    } = this.props;

    if (!amIPresenter) return null;

    return (
       
      
      <div className={styles.container}>
        {
          this.renderPollPanel()
        }
      </div>
    );
  }
}

export default injectIntl(Poll);

Poll.propTypes = {
  intl: PropTypes.shape({
    formatMessage: PropTypes.func.isRequired,
  }).isRequired,
  amIPresenter: PropTypes.bool.isRequired,
  pollTypes: PropTypes.instanceOf(Array).isRequired,
  startPoll: PropTypes.func.isRequired,
  startCustomPoll: PropTypes.func.isRequired,
  stopPoll: PropTypes.func.isRequired,
};
