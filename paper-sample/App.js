import React from 'react';
import { Admin ,Resource} from 'react-admin';
import dataProvider from './core/dataProvider'
import Dashboard from './components/dashboard';
import authProvider from './core/authProvider'
import {LoyaltyList,LoyaltyCreate} from './components/loyaltyTier'
import {LoyaltyPrivlegeList,LoyaltyPrivlegeCreate,LoyaltyPrivlegeShow} from './components/loyaltyTierPrivlege'
import {VoucherList,VoucherShow,VoucherCreate} from './components/vouchers'
import {TierSpendingCreate,TierSpendingList,TierSpendingShow} from './components/tierSpendingRules'
import {SpendingRuleeShow,SpendingRuleList,SpendingRuleCreate} from './components/spendingRules'
import {PointList,PointCreate,PointShow,PointEdit} from './components/pointAccount'
import {SpendingCampList,SpendingShow,SpendingCreate} from './components/spendingCampaign'
import {EarningRuleList,EarningRuleeShow} from './components/earningRules'
import {TierEarningCreate,TierEarningList,TierEarningShow} from './components/tierEarningRules'
import {EarningCampList,EarningCreate,EarningShow,EditEarningCamp} from './components/earningCampaign'
import {CompanyList,CompanyShow} from './components/compnies'
import {SubList} from './components/companySubs'
import {CompanyTierList} from './components/companyLoyaltyTier'
import {PackageList,PackageShow,PackgeEdit} from './components/packages'
import {TransEdit,TransList,TransShow} from './components/pointTransaction'
import {ConsildList,ConsildCreate} from './components/pointConsolidation'
import {RedemList,EditRedem,RedemShow} from './components/voucherRedemption'
import {TypeList} from './components/poinTransactionTypes'
import {CycleList} from './components/cycles'
import {UserCreate,UserList} from './components/users'
import SignUp from './components/signupPage'
import AccountCircle from '@material-ui/icons/AccountCircle';
import Dehaze from '@material-ui/icons/Dehaze';
import Dvr from '@material-ui/icons/Dvr';
import CreditCard from '@material-ui/icons/CreditCard';
import Money from '@material-ui/icons/Money';
import Note from '@material-ui/icons/Note';
import Business from '@material-ui/icons/Business';
import ListAlt from '@material-ui/icons/ListAlt';
import Receipt from '@material-ui/icons/Receipt';
import customRoutes from './core/customRoutes'
import { createBrowserHistory as createHistory } from "history";
const history = createHistory();

const App = () => (
     <Admin title="Paper Admin Panel" loginPage={SignUp} history={history} dashboard={Dashboard} authProvider={authProvider} dataProvider={dataProvider} customRoutes={customRoutes}>
           <Resource name="user"list={UserList} create={UserCreate} icon={AccountCircle}/>
           <Resource name="point-accounts"show={PointShow} list={PointList} create={PointCreate} edit={PointEdit} icon={AccountCircle}/>
           <Resource name="loyalty-tier" list={LoyaltyList} create={LoyaltyCreate} icon={Dehaze} />
           <Resource name="loyalty-tier-privileges" list={LoyaltyPrivlegeList} create={LoyaltyPrivlegeCreate} show={LoyaltyPrivlegeShow} icon={Dvr}/>
           <Resource name="spending-rules" list={SpendingRuleList} create={SpendingRuleCreate} show={SpendingRuleeShow} icon={CreditCard}/>
           <Resource name="vouchers/redemption" options={{ label: 'Redemptions' }}  list={RedemList} show={RedemShow} edit={EditRedem} icon={Note} />
           <Resource name="vouchers" list={VoucherList} create={VoucherCreate} show={VoucherShow} icon={Note} />
           <Resource name="tier-spending-rules" list={TierSpendingList} create={TierSpendingCreate} show={TierSpendingShow} />
           <Resource name="spending-campaigns" list={SpendingCampList} create={SpendingCreate} show={SpendingShow} icon={Money}/>
           <Resource name="earning-rules" list={EarningRuleList} show={EarningRuleeShow} icon={CreditCard} />
           <Resource name="tier-earning-rules" list={TierEarningList} show={TierEarningShow} create={TierEarningCreate}/>
           <Resource name="earning-campaigns" list={EarningCampList} show={EarningShow} create={EarningCreate} edit={EditEarningCamp}icon={Money}/>
           <Resource name="companies/companySubscriptions" options={{ label: 'Subscriptions' }} list={SubList}icon={Business}/>
           <Resource name="companies/loyalty-tier" list={CompanyTierList} show={CompanyShow} icon={Business}/>
           <Resource name="companies" list={CompanyList} show={CompanyShow} icon={Business}/>
           <Resource name="packages" list={PackageList} show={PackageShow} edit={PackgeEdit} icon={ListAlt}/>
           <Resource name="point-transactions/types" options={{ label: 'Transaction Types' }} list={TypeList}/>
           <Resource name="point-transactions/consolidation" options={{ label: 'Consolidations' }} list={ConsildList} create={ConsildCreate}/>
           <Resource name="point-transactions" list={TransList} show={TransShow} edit={TransEdit}icon={Receipt}/>
           <Resource name="cycles" list={CycleList}icon={Receipt}/>
           <Resource name="promotions" list={CycleList} icon={Receipt}/>
           <Resource name="validate-campaign" list={CycleList} icon={Receipt}/>
           <Resource name="validate-voucher" list={CycleList} icon={Receipt}/>

    </Admin>
)

export default App;