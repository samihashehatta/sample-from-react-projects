
import React from 'react';
import { ShowButton } from 'react-admin';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const CommentGrid = ({ ids, data }) => (
  <div style={{ margin: '1em' }}>
  { ids.length>1?
  
        <TableContainer component={Paper}>
        <Table  size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell align="left">id</TableCell>
              <TableCell align="left">spendingRuleId</TableCell>
              <TableCell align="left">name</TableCell>
              <TableCell align="left">partnerId</TableCell>
              <TableCell align="left">currency</TableCell>
              <TableCell align="left">voucherType</TableCell>
              <TableCell align="left">minTransactionValue</TableCell>
              <TableCell align="left">pctVoucherDicount</TableCell>
              <TableCell align="left">maxLimit</TableCell>
              <TableCell align="left">globalDailyLimit</TableCell>
              <TableCell align="left">Details</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
          {ids.map(id =>
              <TableRow key={id}>
                <TableCell component="th" scope="row">
                  {data[id].id}
                </TableCell>
                <TableCell>{data[id].spendingRuleId}</TableCell>
                <TableCell>{data[id].name}</TableCell>
                <TableCell>{data[id].partnerId}</TableCell>
                <TableCell>{data[id].currency}</TableCell>
                <TableCell>{data[id].voucherType}</TableCell>
                <TableCell>{data[id].minTransactionValue}</TableCell>
                <TableCell>{data[id].pctVoucherDicount}</TableCell>
                <TableCell>{data[id].maxLimit}</TableCell>
                <TableCell>{data[id].globalDailyLimit}</TableCell>
                <TableCell><ShowButton to={`/vouchers?filter={"id":"${data[id].id}"}`}/></TableCell>
              </TableRow>
  )}
           
          </TableBody>
        </Table>
      </TableContainer>
      :null}
  </div>
);


 
const SpendingCampaigns = ({ ids, data }) => (
  <div style={{ margin: '1em' }}>
  { ids.length>1?
        <TableContainer component={Paper}>
        <Table  size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell align="left">id</TableCell>
              <TableCell align="left">tierSpendingRuleId</TableCell>
              <TableCell align="left">name</TableCell>
              <TableCell align="left">fixedPointSpent</TableCell>
              <TableCell align="left">globalDailyLimit</TableCell>
              <TableCell align="left">bannerUrl</TableCell>
              <TableCell align="left">terms</TableCell>
              <TableCell align="left">Details</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
          {ids.map(id =>
              <TableRow key={id}>
                <TableCell component="th" scope="row">
                  {data[id].id}
                </TableCell>
                <TableCell>{data[id].tierSpendingRuleId}</TableCell>
                <TableCell>{data[id].name}</TableCell>
                <TableCell>{data[id].fixedPointSpent}</TableCell>
                <TableCell>{data[id].globalDailyLimit}</TableCell>
                <TableCell>{data[id].bannerUrl}</TableCell>
                <TableCell>{data[id].terms}</TableCell>
                <TableCell><ShowButton to={`/spending-campaigns?filter={"id":"${data[id].id}"}`}/></TableCell>
              </TableRow>
  )}
           
          </TableBody>
        </Table>
      </TableContainer>
      :null}
  </div>
);


const makingWithIds = ({location}) =>{

  let promotions=location.state.data.data.promotions
  let vouchersIds = []
  let temp={}
  let vouchersRecords={}
  let campaignsRecords={}
  let campsIds = []
 promotions.vouchers.map((rec)=>{
    vouchersIds.push(rec.id)
    temp = {[rec.id]:rec}
    vouchersRecords ={...vouchersRecords,...temp}
  })
    promotions.spendingCampaigns.map((rec)=>{
    campsIds.push(rec.id)
     temp={[rec.id]:rec}
     campaignsRecords={...campaignsRecords,...temp}

  })
  CommentGrid.defaultProps = {
    data:vouchersRecords,
    ids: vouchersIds
  };  
  SpendingCampaigns.defaultProps = {
    data:campaignsRecords,
    ids: campsIds,
  };
  return true
}

export default function Promotions(props) {

  return (
      <Card>   
        <Title title="Promotions"/>
        <CardContent>
          Vouchers:
          {makingWithIds(props)}
          <CommentGrid/>
          SpendingCampaigns:
          <SpendingCampaigns/>
        </CardContent>
      </Card>
  );
}