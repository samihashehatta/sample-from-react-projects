
import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import { Toolbar, SimpleForm, AutocompleteInput, ReferenceInput, Title, useNotify, Notification } from 'react-admin';
import TextField from '@material-ui/core/TextField';
import { Redirect } from 'react-router-dom'
import dataProvider from '../core/dataProvider';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { set } from 'date-fns';
/* eslint-disable */

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary,
  },
  form: {
    width: '30%', // Fix IE 11 issue.
    marginTop: theme.spacing(7),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  TextField: {
    margin: theme.spacing(3, 0, 2),
  },
  ReferenceInput: {
    margin: theme.spacing(3, 0, 2),
    width: '100%', // Fix IE 11 issue.

  }
}));

export default function Prom() {

  const [user_id, setUser_id] = useState('');
  const [spending_rule_id, setSpending_rule_id] = useState('');
  const [currency, setCurrency] = useState('');
  const [amount, setAmount] = useState('');
  const [company_id, setCompany_id] = useState('');
  const [url, setUrl] = useState('');
  const [response, setResponse] = useState('');

  const notify = useNotify();
  const submit = (e) => {
    console.log(user_id, company_id)
    e.preventDefault();
    dataProvider.create('promotions', {
      data: {
        user_id,
        spending_rule_id,
        currency,
        amount,
        company_id,
      }
    })
      .then(res => {

        setResponse(res)
        setUrl('/get-promotions')
      })
      .catch((err) => {
        notify(`${err}`)
      });


  };
  const classes = useStyles();
  const optionRenderer = choice => `${choice.id} - ${choice.code}`;

  const PostEditToolbar = props => (
    <Toolbar {...props} >
      <Button
        type="submit"
        variant="contained"
        color="primary"
        className={classes.submit}
      >

        Save
            </Button>
    </Toolbar>
  );
  return (
    <Card>
      <Title title="Get Promotions" />
      <CardContent>
        <SimpleForm basePath="localhost:3008/api/v1" toolbar={<PostEditToolbar />} className={classes.form} onSubmit={submit.bind(this)} noValidate>

          <ReferenceInput label="company_id" source="company_id" className={classes.ReferenceInput} reference="companies" onChange={e => setCompany_id(e)} >
            <AutocompleteInput optionText="id" />
          </ReferenceInput>

          <ReferenceInput label="user_id" source="user_id" className={classes.ReferenceInput} reference="point-accounts" filter={{CompanyId:company_id}} onChange={e => setUser_id(e)} >
            <AutocompleteInput optionText="userId" optionValue="userId" options={{error:null}} />
          </ReferenceInput>

          <ReferenceInput label="spending_rule_id" source="spending_rule_id" className={classes.ReferenceInput}reference="spending-rules" onChange={e => setSpending_rule_id(e)}>
            <AutocompleteInput optionText={optionRenderer} />
          </ReferenceInput>
          <TextField
            variant="filled"
            className={classes.TextField}
            fullWidth
            name=" amount"
            label="amount"
            id="amount"
            autoComplete="amount"
            onChange={e => setAmount(e.target.value)}
          />
          <TextField
            variant="filled"
            className={classes.TextField}
            fullWidth
            name="currency"
            label="currency"
            id="currency"
            autoComplete="currency"
            onChange={e => setCurrency(e.target.value)}
          />

        </SimpleForm>
        <Notification />
        {url ? <Redirect to={{ pathname: url, state: response }} /> : null}

      </CardContent>
    </Card>

  );
}
