import { fetchUtils } from 'react-admin';
import { stringify } from 'query-string';
/* eslint-disable */
const apiUrl = process.env.REACT_APP_API_URL
const httpClient =  (url, options = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: 'application/json' });
    }
    // add your own headers here
    options.headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    return fetchUtils.fetchJson(url, options);
}

export default {
    getList: (resource, params) => {
        const { page, perPage } = params.pagination;
        const { field, order } = params.sort;
        let query ={};
        if(params.filter.q) {
             query.filter = JSON.stringify({
                 id:params.filter.q
             })
        } else {
             query = {
                sort: JSON.stringify([field, order]),
                range: JSON.stringify([page,perPage]),
                filter: JSON.stringify(params.filter),
            };
        }
    if(resource === 'promotions') {
        const url = `${apiUrl}/${resource}`;
        if(params.filter){
        return httpClient(url,{
            method:'POST',
            body: JSON.stringify(params.filter)
        }).then(({ headers, body }) => {
            let newBody = JSON.parse(body)
            let total =headers.get('content-range')?parseInt(headers.get('content-range').split('/').pop(), 10): newBody.data.length
            return ({data:newBody.data,total})
        });}
        else return ({data:{},total:0})

    }
        const url = `${apiUrl}/${resource}?${stringify(query)}`;

    return httpClient(url).then(({ headers, body }) => {
        let newBody = JSON.parse(body)
        let total =headers.get('content-range')?parseInt(headers.get('content-range').split('/').pop(), 10): newBody.data.length
        return ({data:newBody.data,total})
    });
    },

    getOne: (resource, params) =>{
       return httpClient(`${apiUrl}/${resource}/${params.id}`).then(({ body }) => {
        let newBody = JSON.parse(body)
          return  ({
            data: newBody.data,
        })})
    },

    getMany: (resource, params) => {
        const query = {
            filter: JSON.stringify({ id: params.ids }),
        };
        const url = `${apiUrl}/${resource}?${stringify(query)}`;
        return httpClient(url).then(({ json }) => ({ data: params.ids }));
    },

    getManyReference: (resource, params) => {
        const { page, perPage } = params.pagination;
        const { field, order } = params.sort;
        const query = {
            sort: JSON.stringify([field, order]),
            range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
            filter: JSON.stringify({
                ...params.filter,
                [params.target]: params.id,
            }),
        };
        const url = `${apiUrl}/${resource}?${stringify(query)}`;
        return httpClient(url).then(({ headers, json }) => ({
            data: params.id,
            total:2,
        }));
    },

    update: (resource, params) =>{
        let api 
        if(resource === 'point-account') api = `${apiUrl}/${resource}/${params.id}/?company_id=${params.data.CompanyId}&user_id=${params.data.userId}`
        else if(resource === 'point-transactions' ) api = `${apiUrl}/${resource}/${params.data.status}/${params.id}`
        else if(resource ==='vouchers/redemption') api = `${apiUrl}/${resource}/${params.data.voucherStatus}/${params.id}`
        else api = `${apiUrl}/${resource}/${params.id}`

      return  httpClient(api, {
            method:resource === 'point-transactions' || resource ==='vouchers/redemption'?'POST': 'PUT',
            body: JSON.stringify(params.data),
        }).then(({ json }) => ({ data: json }))},
    updateMany: (resource, params) => {
        const query = {
            filter: JSON.stringify({ id: params.ids}),
        };
        return httpClient(`${apiUrl}/${resource}?${stringify(query)}`, {
            method: 'PUT',
            body: JSON.stringify(params.data),
        }).then(({ json }) => ({ data: json }));
    },

    create: (resource, params) =>{
        let api ,body;
        if(resource === 'user'){
            api= `${apiUrl}/${resource}/signup`
            body = {
                name:params.data.name,
                email:params.data.email,
                password:params.data.password,
                type:params.data.type,
                status:params.data.status?'active':'suspended'
            }
        }
        else if(resource==="point-transactions/consolidation"){api=`${apiUrl}/${resource}/${params.data.id}`;body=params.data}
        else {api =`${apiUrl}/${resource}` ; body=params.data}
        return httpClient(api, {
            method:'POST',
            body: JSON.stringify(body),
        })
        .then(response => {

        let newBody = JSON.parse(response.body)

            if (response.status < 200 || response.status >= 300) {
                throw new Error(response);
            }
          
            return ({data: newBody})
        })},

    delete: (resource, params) =>
        httpClient(`${apiUrl}/${resource}/${params.id}`, {
            method: 'POST',
        }).then(({ json }) => ({ data: json })),

    deleteMany: (resource, params) => {
        const query = {
            filter: JSON.stringify({ id: params.ids}),
        };
        return httpClient(`${apiUrl}/${resource}?${stringify(query)}`, {
            method: 'DELETE',
            body: JSON.stringify(params.data),
        }).then(({ json }) => ({ data: json }));
    },
};