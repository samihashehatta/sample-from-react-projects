import React from "react";
import { Route } from "react-router-dom";
import Prom from "../components/promotion";
import Test from "../components/getPromotions";
import Promo from "../components/validatePromotion";
import ValidateForm from "../components/promo";
import TransForm from "../components/pointTransactionsFormSpen";
import TransFormRedem from "../components/pointTransactionsFormRedem";
import TransFormAdjOrBon from "../components/pointTransactionsForm";
import PointRedeem from "../components/voucherRedeemPoint";
import SystemForm from "../components/voucherRedemptionSystem";
import ValidateFormVoucher from "../components/promoVoucher";
import PromoVoucher from "../components/validatePromotionVoucher";


export default [
  // <Route exact path="/signup" component={SignUp} noLayout />,
  <Route exact path="/point-transaction-spen" component={TransForm}/>,
  <Route exact path="/point-transaction-redem" component={TransFormRedem}/>,
  <Route exact path="/point-transaction-bonus-adju" component={TransFormAdjOrBon}/>,
  <Route exact path="/voucher-redem-system" component={SystemForm}/>,
  <Route exact path="/voucher-redem-point" component={PointRedeem}/>,
  <Route exact path="/get-promotions" component={Test}  />,
  <Route exact path="/promotions" component={Prom}  />,
  <Route exact path="/validate-campaign" component={ValidateForm}  />,
  <Route exact path="/validate-voucher" component={ValidateFormVoucher}  />,
  <Route exact path="/validate" component={Promo}  />,
  <Route exact path="/validated-voucher" component={PromoVoucher}  />
];
