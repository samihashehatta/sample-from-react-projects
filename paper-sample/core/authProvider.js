const url = process.env.REACT_APP_API_URL
const authProvider = {
    login: ({ email, password }) =>  {
        const request = new Request(`${url}/user/login`, {
            method: 'POST',
            body: JSON.stringify({ email, password }),
            headers: new Headers({ 'Content-Type': 'application/json' }),
        });
        return fetch(request)
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then((response) => {
                localStorage.setItem('token', response.data.token);
            })},
    signup:({email,password,type,name}) =>  {
        const request = new Request(`${url}/user/signup`, {
            method: 'POST',
            body: JSON.stringify({ email, password ,type,name}),
            headers: new Headers({ 'Content-Type': 'application/json' }),
        });
        return fetch(request)
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response);
                }
                return Promise.resolve({ redirectTo: '/login' })
            })
            .then((response) => {
                return Promise.resolve({ redirectTo: '/login' })
            })},
    logout:  () => {
        localStorage.removeItem('token');
        return Promise.resolve();
    },
    checkAuth:() => localStorage.getItem('token')
    ? Promise.resolve()
    : Promise.reject({ redirectTo: '/login' }),

    checkError:  (error) => {
        const status = error.status;
        if (status === 401 || status === 403) {
            localStorage.removeItem('token');
            return Promise.reject();
        }
        return Promise.resolve();
    },
    getPermissions: params => Promise.resolve(),
};
export default authProvider